angular.module("store")
    .constant("dataUrl","http://localhost:5050/products")
    .constant("orderUrl","http://localhost:5050/orders")
    .controller("StoreCtrl", function ($scope, $http, $location, dataUrl, orderUrl, cart) {
        $scope.data = {};

        $http.get(dataUrl)
            .success(function(data){
                $scope.data.products = data;
            })
            .error(function(error){
                err = {};
                err.status = error.status || 404;
                err.message = error.message || "Shit's on fire, yo";
                $scope.data.error = err;
            });

        $scope.sendOrder = function(shippingDetails){
            console.log(shippingDetails);
            var order = angular.copy(shippingDetails);
            order.products = cart.getProducts();
            $http.post(orderUrl, order)
                .success(function(data){
                    $scope.data.orderId = data.id;
                    cart.getProducts().length = 0;
                })
                .error(function(error){
                    $scope.data.orderError = error;
                })
                .finally(function(){
                    $location.path("/complete");
                })
        };
});
