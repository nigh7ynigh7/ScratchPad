angular.module('store')
    .constant('productListActive', 'btn-primary')
    .constant('listPageCount', 10)
    .controller('ProductListCtrl', function($scope, $filter, productListActive, listPageCount, cart){
            var selectedCat = null;

            $scope.selectedPage = 1;
            $scope.pageSize = listPageCount;
            $scope.selectCategory = function(newCat){
                selectedCat = newCat;
                $scope.selectedPage = 1;
            };

            $scope.selectPage = function(newPage){
                $scope.selectedPage = newPage;
            };

            $scope.categoryFilterFn = function(product){
                return selectedCat == null || product.category == selectedCat;
            };

            $scope.getCategoryClass = function(cat){
                return selectedCat == cat ? productListActive : "";
            };

            $scope.getPageClass = function(page){
                return $scope.selectedPage == page ? productListActive : "";
            };

            $scope.addProductToCart = function(product){
                console.log(product);
                cart.addProduct(product.id, product.name, product.price);
            };
    });
