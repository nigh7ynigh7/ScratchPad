//settings available:
var torrent     = false;
var quality     = '720';
var currentTab  = 0;     //slect which tab to start (after current tab): ZERO BASED
var range       = -1;    //how many tabs to dl, -1 for all, 0 for current, >1 for more than 1
var waitBetween = 10000; //time to wait between downloads

/**
 * Input : none
 *
 * Finds the tag corresponding to the options above
 *
 * return : the tag
 */
var findDownloadLinks = function()
{
    //fetch all a tags in the document
    var aTag = document.getElementsByTagName('a');
    var result = null;

    //look for the right tag
    for (var i = 0; i < aTag.length; i++)
    {
        var attribute = aTag[i].getAttribute('href');
        if (attribute.includes('mp4') && attribute.includes(quality))
        {
            if(attribute.includes('torrent') && torrent)
                result = aTag[i];
            else if(attribute.includes('download') && !torrent)
                result = aTag[i];
        }
    }
    //returns the element
    return result;
}


/**
 * input : nothing
 *
 * clicks the next tab based on currentTab
 * will download range
 *
 * return : nothing
 */
var nextTab = function()
{
    //ups tab counter
    currentTab ++;
    //check to see if in ranbge
    if(range > 0)
        range -- ;
    if(range != -1)
        if(range == 0)
            return false;
    //get tab element
    var tab = document.getElementById('tab_' + currentTab);
    //if tab is null, return false
    if (tab == null)
    {
        return false;
    }
    //click tab and return true
    tab.click();
    return true;
}

// set the download variable to null
var download = null;

/**
 * executes an anonymous function every waitBetween milliseconds.
 * if there's no tab to be clicked, terminate the interval
 */
download = setInterval(function(){
    // look for download link and click it
    var link = findDownloadLinks();
    if(!!link)
        link.click();
    //go to next tab
    var next = nextTab();
    //stop interval is there is no tab
    if(!next)
    {
        clearInterval(download);
        alert('Done');
    }
},waitBetween);
