var toBinaryInt = function(num){
    num = Math.floor(num);
    ans = '';

    temp = num;

    while (temp > 1) {
        ans = (temp%2) + ans;
        temp = Math.floor(temp/2);
    }
    if (num == 0) {
        ans = '0';
    }
    else {
        ans = '1' + ans;
    }
    return ans;
}

var toWhole = function(num, pow){
    if (!pow) {
        pow = 2;
    }
    var i = 0;
    while ((Math.pow(pow,i)*num)%2!=0) {
        i++;
    }
    return {result:Math.pow(pow,i)*num, iterations: i};
}

var insertFrontChar = function(val, char, length){
    while(val.length < length){
        val = char + val;
    }
    return val;
}

var toBinaryAny = function(num){
    var isNeg = num < 0;
    var whole = Math.floor(Math.abs(num));
    var frac  = Math.abs(num) - whole;

    whole = toBinaryInt(whole);
    frac = toWhole(frac);
    frac.binary = toBinaryInt(frac.result);
    frac = insertFrontChar(frac.binary, '0', frac.iterations);

    return result = (isNeg ? '-':'') + whole + '.' + frac;
}

// console.log(toBinaryAny(5));
// console.log(toBinaryAny(7));
console.log(toBinaryAny(Math.PI));
console.log(toBinaryAny(3.1));
console.log(toBinaryAny(3));
console.log(toBinaryAny(9.25));
console.log(toBinaryAny(12357.07));


//moral of the story
/*
    when copmaprinh floats or doubles, don't be stupid and test it like this:
        float1 == float2
    test it like this:
        Math.abs(float1 - float2) < 0.000001

            what's going on here?
                your checking the range between these 2 numbers
                floats might vary because they are approximations
                    approximations are not absolute, k?
                so, your checking the difference between these 2 approximations
                afterwards, it will check to see the difference between the 2
                  is as approximate as wanted.

    so be careful while using these numbers

    they will consfuse if you're not ready/prepared
*/
