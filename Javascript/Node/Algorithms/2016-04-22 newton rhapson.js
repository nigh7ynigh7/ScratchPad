var sqrt = function(val){
    var epsilon = 0.00001,
        guess = val/2;

    var i = 0;
    while(Math.abs(guess*guess - val) >= epsilon){
        guess = guess - (guess*guess - val)/(2*guess);
        i++;
    }
    console.log('for value', val, 'there were', i, 'num of guesses. Answer:',guess);
    return guess;
}

sqrt(10);
sqrt(123);
sqrt(17);
sqrt(5);
sqrt(25);
sqrt(4);
sqrt(8);
sqrt(17);
sqrt(81);
sqrt(1237);
sqrt(102394734);
sqrt(139479834594);
