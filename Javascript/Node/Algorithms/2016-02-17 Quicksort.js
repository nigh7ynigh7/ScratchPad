function partition(array, high, low){
    var pivot = array[high];
    var i = low;
    for(var j = low; j < high - 1; j++){
        if (array[j] <= pivot) {
            var temp = array[i];
            array[j] = array[i];
            array[j] = temp;
            i++;
        }
    }
    return i;
}


function quicksort (array, low, high){
    if(low < high){
        var p = partition(array, low, high);
        quicksort(array, low, p-1);
        quicksort(array, p+1, high);
        console.log(array.toString());
    }
}


var array = [1,3,6,2,6,8,3,9,35,5,12,4,3,1,56,1,56,8,3,11,34,52,36,5,0];
quicksort(array,1, array.length);
console.log(array.toString());

/*
    failure here with this experiment, will have to dig deeper with algorithms
*/
