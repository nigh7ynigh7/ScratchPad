var compare = function(val1, val2){
    return val1 < val2;
}

var merge = function (left, right, comparison){
    var result = [];
    var i, j;
    i = j = 0;
    while(i < left.length && j < right.length){
        if (comparison(left[i], right[j])) {
            result.push(left[i++]);
        } else {
            result.push(right[j++]);
        }
    }
    while (i < left.length) {
        result.push(left[i++]);
    }
    while (j < right.length) {
        result.push(right[j++]);
    }
    return result;
}

var sort = function(list){
    console.log(list);
    if(list.length <= 1){
        return list;
    }
    var mid = Math.floor(list.length/2);
    return merge(
        sort(list.slice(0, mid)),
        sort(list.slice(mid)),
        compare);
}

var arr = [2,4,6,3,67,34,78,43,3,57,5,24,88,8,89,3];
var srt = sort(arr);
console.log(srt);
