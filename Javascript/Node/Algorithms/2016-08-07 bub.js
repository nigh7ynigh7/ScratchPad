function Swap(arr, i, j)
{
    console.log(arr, i, j);
    temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

function BubbleSort(arr)
{
    for (var i = 0; i < arr.length; i++)
    {
        var swap = false;
        for (var j = 1; j < arr.length - i; j++)
        {
            if(arr[j] < arr[j-1])
            {
                swap = true;
                Swap(arr, j, j-1);
            }
        }
        if (!swap)
        {
            break;
        }
    }
}

var ls = [1,2,3,6,4,5];

BubbleSort(ls);

console.log(ls);
