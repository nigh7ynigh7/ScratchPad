function gcd (a,b){
    min = Math.min(a,b);
    max = Math.max(a,b);

    if(min == 0){
        return max;
    }
    return gcd(min, max % min);
}

module.exports = gcd;
