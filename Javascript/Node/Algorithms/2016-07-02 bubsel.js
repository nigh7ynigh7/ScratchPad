/**
 * input : a list, 2 different positions (i and j)
 *
 * swaps 2 elements from position i and j in the informed list
 *
 * returns : null (does it all in memory)
 */
var swap = function (arr, i, j){
    temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
};

/**
 * input : list
 *
 * recieves a reference of a list and orders it using selection sort.
 *
 * return : null (no need to return the modified list - it was dealt with in memory)
 */
var order = function(list){
    //go through the list and swap when necessary
    for (var i = 0; i < list.length-1; i++) {
        //make a swapped indicator
        var wasSwapped = false;
        for (var j = i+1; j < list.length; j++) {
            //swaps 2 elements of a list and indicate that a swapped occured
            if(list[i] > list[j]){
                swap(list, i, j);
                wasSwapped = true;
            }
        }
        //if swap didn't occur, end the function. the list is sorted.
        if(wasSwapped == false)
            return;
    }
};

/**
 * input : list
 *
 * recieves a reference of a list and orders it using bubble sort.
 *
 * return : null (no need to return the modified list - it was dealt with in memory)
 */
var bubble = function(list){
    if(list.length <= 1) return;
    //initialize the control variables
    var exit = false;    //determines when to exit loop
    var iteration = 0;   //checkes which iteration to do
    var swapped = false; //checks to see if a swap occurred during a loop

    //loop to order the list
    while(!exit){
        //sets index comparison and resets swapped variable
        iteration ++;
        var i = iteration % list.length;
        if (i == 0) swapped = false;

        //limits check to list.length-2. No out of bound error.
        if (i < list.length - 1) {
            //swaps current index with the next if needed
            if(list[i] > list[i+1]){
                swap(list, i, i+1);
                swapped = true;
            }
        }

        // determines if the loop should be exited
        exit = i == list.length - 2 && !swapped;
    }
};

//creates an array/list with unordered values

var arr = ["hello", "this", "is", "an", "algorithm",
    "that", "does", "complex", "things", "with", "a",
    "slight", "amount", "of", "love", "and", "mainly",
    "efficiency"];

//calls the function order to order the elements in the previously declared array
// order(arr);
bubble(arr);

/**
 * input : a list and an element that might be in the list
 *
 * Implements a iterative (non recursive) binary search algorithm.
 * While it does reduce the problem by half, it takes up more CPU and less memory.
 *
 * return : a boolean value (true if element is in line, otherwise, false)
 */
var find = function(list, element){
    //decalre high and low points
    var low = 0;
    var high = list.length - 1;

    //loops until high and low are equal --> bound to happen
    while(high != low){
        //find midpoint
        var mid = low + Math.floor((high-low)/2);

        //checks to see if mid index is the right element
        if(list[mid] == element){
            return true;
        }else {
            //sees where to cut the rest off
            if(element < list[mid]){
                high = mid - 1;
            }else {
                low = mid + 1;
            }
        }
    }
    //if nothing was found, return false
    return false;
};


/**
 * input : a list and an element that might be in the list
 *
 * this function will go through the list using recursive binary search.
 * this function is not memory efficient as it creates new arrays after each call.
 *     don't forget about the callstack itself. it creates more arrays with more environments.
 *
 * return : a boolean value (true if element is in line, otherwise, false)
 */
var rFind = function(list, element){
    //selects a mid point
    var mid = Math.floor((list.length-1)/2);

    if(list[mid] == element){   //checks to see if mid point's is the answer.
        return true;
    }else if(list.length == 1){ //if list.length == 1, nothing was found
        return false;
    }
    else {
        //chooses which end of the list to look into
        if(element < list[mid]){
            return rFind(list.splice(0, mid-1), element);
        }else{
            return rFind(list.splice(mid+1), element);
        }
    }
};


/**
 * input : a list and an element that might be in the list
 *
 * this function will go through the list using recursive binary search.
 *
 * return : a boolean value (true if element is in line, otherwise, false)
 */
var rFind2 = function(list, element){
    //check to see if the list empty and return false
    if (list.length == 0) return false;

    //creates a subfunction to recursively do a binary search on the list
    var recur = function(list, element, low, high){
        // when high == low, check to see if
        if(low == high) return list[low] == element;

        //find the midpoint between low and high
        var mid = low + Math.floor((high-low)/2);

        if(list[mid] == element){
            return true;
        }
        else if(element < list[mid]){
            return rFind(list, element, 0, mid-1);
        }else{
            return rFind(list, element, mid+1, high);
        }
    }
    return recur(list, element, 0, list.length-1)
};

//print the ordered array
console.log(arr);
//prints the number step number and the answer of these binary search functions
console.log('1', find(arr,'an'));
console.log('2', rFind(arr,'hello'));
console.log('3', rFind2(arr, 'does'));

/*
 * swap(array, i, j) --- O(1)
 * order(arr) ---------- O(n^2)
 * bubble(arr) --------- O(n^2)
 *
 * find(arr,'an') ------ O(log(n))
 * rFind(arr,'hello') -- O(log(n))
 * rFind2(arr, 'does') - O(log(n))
 */
