
function SolveHanoi(size, from, to, spare){
    if(size === 1)
        console.log('move disc from',from,'to',to);
    else{
        SolveHanoi(size-1, from, spare, to);
        SolveHanoi(1, from, to, spare);
        SolveHanoi(size-1, spare, to, from);
    }
}

SolveHanoi(3,'A', 'C', 'B');
// var numMoves = 0
// for (var i = 1; i <= 64; i++) {
//     numMoves = 2*(numMoves) + 1;
// }
// console.log('moves:',numMoves);
console.log('resetting: ');
SolveHanoi(4,'A', 'C', 'B');
// console.log('resetting: ');
// SolveHanoi(5,'A', 'C', 'B');
