function isPalindrome(string){
    function killWhiteSpace(string){
        return string.toLowerCase().replace(/ /g,"");
    }

    function isPal(string){
        if(string.length <= 1){
            return true;
        }
        return string[0] == string[string.length-1]
            && isPal(string.substr(1, string.length-2));
    }

    return isPal(killWhiteSpace(string));
}

console.log(isPalindrome('race car, ok?'));
