function fibonacci(num){
    if (num===0 || num === 1)
        return num;
    else
        return fibonacci(num-1) + fibonacci(num-2);
}

var string = "";
for (var i = 0; i <= 40; i++) {
    string += fibonacci(i);
    if (i < 40) {
        string += ', ';
    }
}
console.log(string);
