'use strict';
var fs = require('fs');

class File{
    constructor(){
        this._file = null;
    }
    open(path, document){
        fs.readFile(path, 'utf-8',  function(error, contents){
            document.getElementById('editor').value = contents;
        });
    }
    save(path, document){
        var text = document.getElementById('editor').value;
        fs.writeFile(path, text);
    }
}

module.exports = new File();
