﻿using System;

namespace EventHandler
{
	public delegate void NoOuties(string msg);
	public delegate int Inties(int i, string s);
	class MainClass
	{
		public static void Main(string[] args)
		{
			NoOuties no = LogEvent.hello;
			no += LogEvent.say;
			no += x => { Console.WriteLine("status: {0}", x); };
			no("testing");

			Inties it;
			it = (n, s) => { Console.WriteLine("{0}, {1:###-###-####}", n, s); return s.Length; };
			it += (r, s) => { return r * s.Length; };
			int i = it(3, "4109930811");
			//returned value is last function inputted
			Console.WriteLine("answ = {0}", i);
			Console.ReadKey();
		}
	}

	class LogEvent
	{
		public static void hello(string name)
		{
			Console.WriteLine("Hello, " + name);
		}
		public static void say(string whatever)
		{
			Console.WriteLine(whatever);
		}
	}
}
