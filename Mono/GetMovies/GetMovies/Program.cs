﻿using System;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.IO;

namespace GetMovies
{
	class Movie {
		public string Title = "";
		public string Year = "";
		public string imdbID = "";
		public string Type = "";
		public string Poster = "";
	}
	class MovieDB {
		public List<Movie> Search = null;
		public string totalResults = "";
		public string Response = "";
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			string movie = Console.ReadLine ();
			using (var client = new WebClient ()) {

				var contents = client.DownloadString ("http://www.omdbapi.com/?s=" + movie);
				MovieDB db = null;
				try{
					db = JsonConvert.DeserializeObject<MovieDB>(contents);
				}catch(Exception){
					db = new MovieDB ();
					db.totalResults = "";
					db.Response = "False";
				}

				WriteExcel (db);
			}
			Console.ReadKey ();
		}
		public static void WriteExcel(MovieDB db){
			if (db.Search==null || !db.Response.Equals("True")) {
				Console.WriteLine ("Not ok");
				return;
			}
			using (var pkg = new ExcelPackage (new FileInfo (@"output.xlsx"))) {
				//makes worksheet

				var ws = pkg.Workbook.Worksheets.Add ("movies");

				//makes header
				ws.Cells["A1"].Value="Title";
				ws.Cells["B1"].Value="IMDB";
				ws.Cells["C1"].Value="Type";
				ws.Cells["D1"].Value="Year";

				var row = 2;

				foreach (var item in db.Search) {
					try{
						ws.Cells ["A" + row].Value = item.Title;
						ws.Cells ["B" + row].Value = "="+item.imdbID;
						ws.Cells ["C" + row].Value = item.Type;
						ws.Cells ["D" + row].Value = int.Parse(item.Year);
						row++;
					}
					catch(Exception){
						row++;
					}
				}

				pkg.Save ();
				Console.WriteLine ("K, done");
			}
		}
	}
}
