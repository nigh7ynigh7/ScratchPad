﻿using System;

namespace AspNetMVC.Models
{
	public class PlayerRecord
	{
		public int RecordID {
			get;
			set;
		}
		public int Rating {
			get;
			set;
		}
		public string Description {
			get;
			set;
		}
	}
}

