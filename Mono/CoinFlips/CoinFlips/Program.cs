﻿using System;

namespace CoinFlips
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int flipLimits = 1000000000;
			int headCount = 0;
			int tailCount = 0;
			var random = new Random ();

			Console.WriteLine ("Starting....");
			for (int i = 0; i < flipLimits; i++) {

				if (Math.Round (random.NextDouble ()) == 1)
					headCount++;
				else
					tailCount++;

				if (i%1000 == 0 && i != 0) {
					
					Console.Clear ();
					Console.WriteLine ("Total Flips: " + i);
					Console.WriteLine ("Heads: " + headCount);
					Console.WriteLine ("Tails: " + tailCount);
					Console.WriteLine ("Head percentage - {0:P}, Tail percentage {1:P}", (double) headCount/(i+1), (double) tailCount/(i+1));

				}

			}
			Console.ReadKey();
		}
	}
}
