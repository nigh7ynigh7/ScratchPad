# ABOUT

Simple projects go here. Will upload various projects from varying frameworks, languages, etc.

This is all mainly a learning process and I hope that, by the end of this year, this repository is much bigger. With learning, comes greater expertise and wisdom - two things that I always strive to achieve.

So, having said that, clone this repo if you want to. Not a lot of personal information here. And most of it will be clean (language and mannerisms). Enjoy the coding.....

# DIRECTORY

## Mono

All mono projects will go under this folder/hierarchy. As for the projects themselves - all of the were made with Xamarin Studio (MonoDevelop). So, you can use that to open it up. Please make sure the nuget references are all corrected before you run these projects.

* `GetMovies` : not a pirating app, don't worry about it - it's something that fetches movie info from OMDB API based on a search string and exports the result as an `.xlsx` Excel file.
* `ExcelRandomizeGrid` : I wanted to fill up all of the cells in the Excel grid, not a lot of luck here - it would generally crash along the way, so I decided no to through with it. Even creating a 1000x1000 document takes a while. (I really wanted that 1gb `.xlsx` file....)
* `CoinFlips` : Just testing an idea - result of a series of coin flips will eventually mirror the probability of landing either heads or tails.
