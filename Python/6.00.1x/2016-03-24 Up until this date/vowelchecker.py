sent = raw_input("Type something: ");
vowels = ['a','e','i','o','u'];
has = False;
for i in range(0, len(sent)):
    for v in vowels:
        if sent[i] == v:
            has = True;
            break;
if has:
    print 'This has a vowel';
else:
    print 'this has no vowels';
