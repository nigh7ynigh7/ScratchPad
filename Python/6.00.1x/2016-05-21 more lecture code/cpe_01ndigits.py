def ndigits(x):
    if(x == 0):
        return 1
    def calc(x):
        x = abs(x)
        if x == 0:
            return 0;
        return 1 + calc(int(x/10))
    return calc(x)
