arr = ["The Holy Grail", 1975, "Terry Jones & Terry Gilliam", 91, ["Graham Chapman", ["Michael Palin", "John Cleese", "Terry Gilliam", "Eric Idle", "Terry Jones"]]];
for i in arr:
    if isinstance(i,list):
        for j in i:
            if isinstance(j, list):
                for k in j:
                    print("\t\t"+k);
            else:
                print("\t"+j);
    else:
        print(i);
