def evalQuadratic(a,b,c,x):
    return a*x**2 + b*x + c

print str(evalQuadratic(1,2,3,4))
print str(evalQuadratic(0,0,0,1))
print str(evalQuadratic(1,1,1,1))
print str(evalQuadratic(2,5,4,5))
print str(evalQuadratic(9,9,9,9))
