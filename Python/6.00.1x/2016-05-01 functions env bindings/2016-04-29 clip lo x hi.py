def clip(lo, x, hi):
    '''
        Takes in three numbers and returns a value based on the value of x.
        Returns:
         - lo, when x < lo
         - hi, when x > hi
         - x, otherwise
    '''
    z = min(x, hi)
    return max(lo, z)

print str(clip(1,2,3))
print str(clip(3,2,1))
print str(clip(5,1,5))
print str(clip(4,1,4))
