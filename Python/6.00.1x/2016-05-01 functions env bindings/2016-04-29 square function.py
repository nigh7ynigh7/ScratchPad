def square(x):
    if type(x) == int or type(x) == float:
        return x**2
    return 0;

print str(square(1))
print str(square(2))
print str(square(3))
print str(square(5.4))
print str(square(3.14159265))
print str(square(None))
print str(square(True or False))
print str(square('Game of townes'))
