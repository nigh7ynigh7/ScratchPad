array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, [1, 2, 3, 4, 5, 6, 7, 8, 9, [1, 2, 3, 4, 5, 6, 7, 8, 9, [1, 2, 3, 4, 5, 6, 7, 8, 9]]]];

def printArray(array, level):
    for i in array:
        if(isinstance(i, list)):
            printArray(i, level+level);
        else:
            print(level+str(i));

printArray(array, '\t');
