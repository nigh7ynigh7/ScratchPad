from math import sqrt


class Coordinate(object):
    def __init__(self, x = 0, y = 0):
        self.x = x;
        self.y = y;
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")";
    def distance(self, other):
        return sqrt((self.x - other.x)**2 + (self.y - other.y)**2)

c = Coordinate(2,3);
print (c.distance( Coordinate(5,7)));
