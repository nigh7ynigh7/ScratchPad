def hexValue(value):
    if value < 10:
        return str(value)
    elif value < 16:
        return {
            10:'a',
            11:'b',
            12:'c',
            13:'d',
            14:'e',
            15:'f'}[value]
    else:
        return ''

def convertToHex(value):
    answer = ''

    while value >= 16:
        answer = hexValue(value%16)+answer
        value = value/16
        
    answer = hexValue(value%16) + answer

    return answer;

print(convertToHex(2748))