
def isIn(char, aStr):
    midPoint = len(aStr)/2;
    # print aStr + ' -- ' + str(midPoint) + ' ' + str(len(aStr))
    if(aStr == ''):
        return False
    if(char == aStr[midPoint]):
        return True
    elif len(aStr) == 1:
        return False
    else:
        if char < aStr[midPoint]:
            return isIn(char, aStr[0:midPoint])
        return isIn(char, aStr[midPoint:])

print isIn('i', 'adfgnusvz')
