def lenIter(aStr):
    i = 0;
    if aStr == '':
        return 0
    i+=1
    while(aStr[0:i*-1] != ''):
        i+=1;
    return i

def lenRecur(aStr):
    if(aStr == ''):
        return 0;
    return 1 + lenRecur(aStr[0:-1])
print lenRecur("33")
