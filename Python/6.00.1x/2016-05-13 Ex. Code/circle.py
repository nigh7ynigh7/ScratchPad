pi = 3.14159265

def area(radius):
    return pi*radius**2

def circumference(radius):
    return 2*pi*radius

def sphereSurface(radius):
    return 4.0*area(radius)

def sphereVolume(radius):
    """
        :param radius :type number radius of the circle

        Uses the radius to calculate the volume of the circle

        :returns volume of the sphere
    """
    return(4.0/3.0)*pi*(radius**3)

