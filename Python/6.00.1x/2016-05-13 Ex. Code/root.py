def rootFinder(value, root, precision):
    low = 0.0;
    high = value;
    guess = (low+high)/2.0;
    i = 0;
    while (abs(guess**root - value) > precision):
        if guess**root < value:
            low = guess;
        else:
            high = guess;
        guess = (low+high)/2.0;
        i += 1;
    print 'tries:'+ str(i);
    return guess;

print str(rootFinder(16,4,0.00000001))