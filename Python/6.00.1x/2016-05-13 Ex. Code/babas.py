def recurPower(base,exp):
    if exp == 0:
        return 1
    # return base * recurPower(base, exp-1)
    if exp > 0 and exp%2 == 0:
        return recurPower(base*base, exp / 2)
    if exp > 0 and exp%2 == 1:
        return base * recurPower(base, exp -1)

print recurPower(2,7)