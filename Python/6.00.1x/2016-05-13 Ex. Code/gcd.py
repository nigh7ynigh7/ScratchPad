def gcd(a,b):
    mn = min(a,b)
    mx = max(a,b)
    if(mn == 0):
        return mx;
    return gcd(mn, mx % mn)

print gcd (16,24);