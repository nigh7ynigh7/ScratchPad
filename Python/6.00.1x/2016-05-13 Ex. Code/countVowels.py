def isVowel(char):
    return char.lower() in 'aiueo'


def countBob(s):
    c = 0
    for i in range(0, len(s)-2):
        if (s[i] + s[i + 1] + s[i + 2] == 'bob'):
            c += 1
    return c


def countOccurences(s, t):
    c = 0
    for i in range(0, len(s)-len(t)+1):
        temp = ''
        for j in range(0, len(t)):
            temp += s[i+j]
        if(temp == t):
            c+=1
    return c


def countVowels(string):
    c = 0
    for s in string:
        if isVowel(s):
            c += 1
    return c


def item_order(order):
    hamburger = countOccurences(order, 'hamburger')
    salad = countOccurences(order, 'salad')
    water = countOccurences(order, 'water')

    return "salad:"+str(salad)+" hamburger:"+str(hamburger)+" water:"+str(water)

# print ('Number of Vowels: ' + str(countVowels(raw_input('Gib a string: '))))

print ('numbers of bobs in bobobo bo bobobo: '+str(countBob('bobobo bo bobobo')))

print (item_order("hamburger hamburger hamburger water salad salad hamburger water water salad"))