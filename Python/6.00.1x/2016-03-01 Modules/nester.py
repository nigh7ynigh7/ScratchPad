def printArray(array, level):
    for i in array:
        if(isinstance(i, list)):
            printArray(i, level+level);
        else:
            print(level+str(i));
