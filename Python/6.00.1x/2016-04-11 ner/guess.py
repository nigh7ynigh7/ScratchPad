low = 0;
high = 100;
guess = 0;
usr = '';
print "Please think of a number between 0 and 100!";

while True:
    guess = int((low+high)/2);
    print("Is your secret number " + str(guess) + "?");
    usr = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ");

    if usr == 'l':
        low = guess;
    elif usr == 'h':
        high = guess;
    elif usr == 'c':
        break;
    else:
        print "Sorry, I did not understand your input."

print("Game over. Your secret number was: "+str(guess));
