val = float(raw_input('Number between 0 and 1'));
if val < 1 and val > 0:
    counter = 0;

    while ((2**counter)*val)%2 != 0:
        counter += 1

    print counter
else:
    print 'Invalid number'