num = 3
result = ''
isNeg = False

if num < 0:
    isNeg = True
    num = abs(num)
else:
    isNeg = False
if num == 0:
    result = '0'
while num > 2:
    result = str(num%2)+str(result)
    num = num/2;
if isNeg:
    result = '-' + result

print result;