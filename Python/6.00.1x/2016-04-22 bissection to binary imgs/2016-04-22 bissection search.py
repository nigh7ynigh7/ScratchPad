x = float(raw_input('Gib num: '))
eps = 0.01
guesses = 0
low = 0
high = x
ans = (low + high)/2.0
while (ans**2 - x) >= epsilon:
    guesses += 1
    is ans**2 < x:
        low = ans
    else
        high = ans
    ans = (high + low)/2.0
print ('num of guesses: ' + str(guesses))
print ('answer is: ' + str(ans))
