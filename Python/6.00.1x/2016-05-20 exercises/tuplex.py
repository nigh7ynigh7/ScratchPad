def oddTuples(aTup):
    odd = True
    result = ()
    for i in aTup:
        if(odd):
            result += (i,)
        odd = not odd
    return result

print(oddTuples((1,2,'three',4.0,'is', 'a', 'stupid', 'dog')));
